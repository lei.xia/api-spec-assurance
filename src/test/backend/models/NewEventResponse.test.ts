import {
    NewEventResponse,
    NewEventResponseTargetEnum
} from "../../../../target/generated/typescript/v1.0/example-spec-openapi/models/NewEventResponse";

describe("New event response", () => {
    it("should contain all required properties", () => {
        expect(NewEventResponse.getAttributeTypeMap()).toEqual([
            {
                "name": "id",
                "baseName": "id",
                "type": "string",
                "format": "uuid"
            },
            {
                "name": "timestamp",
                "baseName": "timestamp",
                "type": "Date",
                "format": "date-time"
            },
            {
                "name": "target",
                "baseName": "target",
                "type": "NewEventResponseTargetEnum",
                "format": ""
            },
            {
                "name": "message",
                "baseName": "message",
                "type": "string",
                "format": ""
            }
        ]);
    })
})
