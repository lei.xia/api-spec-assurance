import {
    NewEventRequest
} from "../../../../target/generated/typescript/v1.0/example-spec-openapi/models/NewEventRequest";


describe("New event request", () => {
    it("should define all required properties", () => {
        expect(NewEventRequest.getAttributeTypeMap()).toEqual([
            {
                "name": "id",
                "baseName": "id",
                "type": "string",
                "format": "uuid"
            },
            {
                "name": "message",
                "baseName": "message",
                "type": "string",
                "format": ""
            },
            {
                "name": "target",
                "baseName": "target",
                "type": "string",
                "format": ""
            }
        ])
    })
})
