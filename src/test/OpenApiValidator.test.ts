import SwaggerParser from "@apidevtools/swagger-parser";
import path from "path";

describe("API specification", () => {
    it('example api spec is valid', () => {
        const root = path.resolve(__dirname);
        SwaggerParser.validate(`{root}/../../api-spec-assurance/specs/example-spec-openapi.yaml`,
            (err) => {
            expect(err).toEqual(null);
        })
    })
})
