const baseExampleClasses = '!target/generated/typescript/v1.0/example-specification-openapi/models';

module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    collectCoverage: true,
    collectCoverageFrom: [
        'target/**/*.ts',
        `${baseExampleClasses}/*.ts`,
        '!target/**/*AllOf*.ts',
    ],
    moduleFileExtensions: ['js', 'ts'],
    coverageDirectory: 'coverage',
    coverageThreshold: {
        global: {
            branches: 0,
            functions: 0,
            lines: 0,
            statements: 0,
        },
        './target/**/*.ts': {
            branches: 100,
            functions: 0,
            lines: 0,
            statements: 100,
        },
    },
    verbose: true,
    testPathIgnorePatterns: [
        '/node_modules/',
    ],
    roots: ['src', 'target/generated/typescript'],
};
