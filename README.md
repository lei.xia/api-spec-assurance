## Overview

Example open api specs

Services documented:

-   /specs/example-spec-openapi.yaml

## Generating and testing
To run the generation and test stage:
```shell
npm run generate:test
```

> **_NOTE:_** If the tests fail, don't just fix them. Failure of these tests indicates that a breaking change has been
made to the API. Talk to anyone consuming the API before deciding on the action to take.

## Previewing your API

To preview whilst editing, use the following (substitute the appropriate spec filename in SWAGGER_FILE):

Run the script `./product-docs/build/local-openapi.sh`

Then:
```shell
  open openapi-spec/example-spec-openapi.html
```

### Backend API
```shell
docker pull swaggerapi/swagger-editor
docker run -p 80:8080 -v $(pwd)/specs/v1.0/:/tmp -e SWAGGER_FILE=/tmp/example-spec-openapi.yaml swaggerapi/swagger-editor
open http://localhost
```

## Typescript notes
If you plan to use the openAPI generator to create your Typescript files, note that it will create unused imports. These will be rejected by
the linter.  To deal with this use the configuration from the `.eslintrc.js` file in this project.

For example, assuming you copied `.eslintrc.js` to `openapi-fix.js` and your generated output ends up in `target/generated/typescript`:
```shell
npx eslint --no-eslintrc --fix --config ./openapi-fix.js target/generated/typescript
```

## Committing

To use a wizard-style commit message generator for changes:
1. Run `npm ci`
1. Then run `npm run cm`

This will walk through a conventional commit questionnaire.
